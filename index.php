<!DOCTYPE html>
<html>
  <head>
    <link href='https://fonts.googleapis.com/css?family=Oswald&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <meta charset="utf-8">
    <title>
      Progo.biz
    </title>

    <style media="screen">
      body, html {
        position: relative;
        height: 100%;
        font-family: 'Oswald', sans-serif;
        background-color: white;
      }
      .p {
        color: rgba(52, 129, 175, 0.53);
        font-size: 35px;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translateX(-50%) translateY(-50%);
        margin-top: -10px;
        text-align: center;
      }
      img {
        width :60%;
      }
      .contact-outer {
        border-bottom: 10px solid rgba(52, 129, 175, 0.53);
        border-top: 10px solid rgba(52, 129, 175, 0.53);
        padding: 30px 55px;
      }
      .contact-inner {
        font-size: 19px;
        color: rgba(0, 0, 0, 0.61);
        font-style: italic;
      }
    </style>

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet">
  </head>
  <body>

    <div class="p">
      <img src="/0.jpeg" alt="" />
      <p class="contact-outer">
        Sitemiz Güncellenmektedir
        <span class="contact">
          <p class="contact-inner"><i class="fa fa-map-marker fa-fw"></i> MANSUROĞLU MAH. 286/4 SOK. NO:2 D:1 DEFNE PLAZA BAYRAKLI / İZMİR </p>
          <!-- <p class="contact-inner"><i class="fa fa-at fa-fw"></i>  Lorem Impsum </p> -->
          <p class="contact-inner"><i class="fa fa-phone fa-fw"></i>0 (232) 435 20 02</p>
        </span>
      </p>
    </div>
  </body>
</html>
